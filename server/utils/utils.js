const uuid = require("uuid");

const getUUID = () => {
  let str = uuid.v1();
  return str.substr(1, 34).replace(/-/g,"");
}
const ErrorMap = (error) => {
    return error // we can add a JSON error mapping if necessary
}

const SuccessMap = (data) => {
    return data    // map data to the required format here....
}

const isDate = function(date) {
    return (new Date(date) !== "Invalid Date") && !isNaN(new Date(date));
}

exports.ErrorMap = ErrorMap;
exports.SuccessMap = SuccessMap;
exports.isDate = isDate;
exports.getUUID = getUUID;
