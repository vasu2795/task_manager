module.exports = (sequelize, DataTypes) => {
  const tasklogs = sequelize.define('tasklogs', {
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    task_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    logged_on: {
      type: DataTypes.DATE,
      allowNull: false,
      default: DataTypes.NOW
    }
  },{
    timestamps: false
  });
  tasklogs.associate = (models) => {
    tasklogs.belongsTo(models.tasks,{
      foreignKey: 'task_id',
      as: 'task',
    });
    tasklogs.belongsTo(models.users,{
      foreignKey: 'user_id',
      as: 'user',
    });
  };
  return tasklogs;
};
