module.exports = (sequelize, DataTypes) => {
  const stages = sequelize.define('stages', {
    stage: {
      type: DataTypes.STRING,
      allowNull: false
    },
    level: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  },{
    timestamps: false
  });
  return stages;
};
