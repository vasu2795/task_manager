module.exports = (sequelize, DataTypes) => {
  const tasks = sequelize.define('tasks', {
    heading: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
    user_id: {
      type: DataTypes.ENUM("USER", "ADMIN"),
      allowNull: true
    },
    due_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    stage_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  },{
    timestamps: false
  });
  tasks.associate = (models) => {
    tasks.hasMany(models.subtasks, {
      foreignKey: 'task_id',
      as: 'subtasks',
    });
    tasks.hasMany(models.tasklogs, {
      foreignKey: 'task_id',
      as: 'tasklog',
    });
    tasks.belongsTo(models.stages, {
      foreignKey: 'stage_id',
      as: 'stages',
    });
    tasks.belongsTo(models.users, {
      foreignKey: 'user_id',
      as: 'user',
    });

  };
  return tasks;
};
