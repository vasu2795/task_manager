module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define('users', {
    username: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    role: {
      type: DataTypes.ENUM("USER", "ADMIN"),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true
    }
  },{
    timestamps: false
  });
  users.associate = (models) => {
    users.hasMany(models.subtasks, {
      foreignKey: 'user_id',
      as: 'subtasks'
    });
    users.hasMany(models.tasks, {
      foreignKey: 'user_id',
      as: 'tasks'
    });
  };
  return users;
};
