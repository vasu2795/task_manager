const stages = require('./').stages;

module.exports = (sequelize, DataTypes) => {
  const subtasks = sequelize.define('subtasks', {
    heading: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
    task_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    // stage_id: {
    //   type: DataTypes.INTEGER,
    //   references: {
    //     model: stages, // Can be both a string representing the table name, or a reference to the model
    //     key:   "id"
    //   }
    // },

    stage_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  },{
    timestamps: false
  });
  subtasks.associate = (models) => {
    console.log("modelling...")
    subtasks.belongsTo(models.stages,{
      foreignKey: 'stage_id',
      as: 'stage',
    });
    subtasks.belongsTo(models.tasks, {
      foreignKey: 'task_id',
      as: 'task',
    });
    subtasks.belongsTo(models.users, {
      foreignKey: 'user_id',
      as: 'user',
    });

  };
  return subtasks;
};
