const redis   = require("redis");
const env = process.env.NODE_ENV || 'development';
const config = require(`${__dirname}/../config/config.json`)[env];
const redisClient = redis.createClient({host : config.redishost, port : config.redisport});

isAdmin = function(req, res, next) {
  console.log("is admin ?? ");
  verifyToken(req,res,next,"ADMIN");
};

isUser = function(req,res,next) {
  verifyToken(req,res,next,"USER");
};

isValid = function(req,res,next) {
  verifyToken(req,res,next);
};

verifyToken = function(req, res, next, required_role = null) {

  redisClient.get(req.headers['x-userid'] + "_loginToken",function(err,response){
    console.log("qwerty qwerty",req.headers['x-userid'] + "_loginToken");
    if(err) {
      return res.status(400).send({ error: true, message: "Invalid Headers" });
    }  else if(response) {
      console.log("response is ",response);
      let resp = response.split('_')
      let token = resp[0];
      let role = resp[1];
      console.log("requred role  and role ",required_role,role,token);
      if(required_role != role  && required_role != null) {
        return res.status(400).send({ error: true, message: "Invalid Access" });
      }
      if(token == req.headers['x-logintoken']) {
        console.log("x-role set ")
        req.role = role;
        console.log(req.role);

        next();
      }
      else {
        console.log("not going to next");
        return res.status(400).send({ error: true, message: "Invalid Headers" });
      }
    } else {
      console.log("else going to next");
      return res.status(400).send({ error: true, message: "Invalid Headers" });
    }
  });
};

exports.isAdmin = isAdmin;
exports.isUser = isUser;
exports.isValid = isValid;
