const UsersController = require('../controllers').users;
const TasksController = require('../controllers').tasks;
const StagesController = require('../controllers').stages;
const SubTasksController = require('../controllers').subtasks;
const isAdmin = require('../middleware/logintokenauth.js').isAdmin;
const isUser = require('../middleware/logintokenauth.js').isUser;
const isValid = require('../middleware/logintokenauth.js').isValid;

module.exports = (app) => {
  app.get('/', (req, res) => res.status(200).send({
    message: 'Welcome to the Task Manager!',
  }));

  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-LoginToken, X-UserId,Version, Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");

    next();
  });

  app.get('/users',isValid,UsersController.list);
  app.get('/users/:userId',isValid,UsersController.retrieve);
  app.post('/users/login',UsersController.login);
  app.post('/users/logout',isValid,UsersController.logout);
  app.post('/users/create',isAdmin,UsersController.create);
  app.post('/users/:userId',isAdmin,UsersController.update);
  app.delete('/users/:userId',isAdmin,UsersController.remove);

  app.get('/tasks/:taskId',isValid, TasksController.retrieve);
  app.get('/tasks',isValid, TasksController.list);
  app.post('/tasks/create',isAdmin,TasksController.create);
  app.post('/tasks/:taskId',isValid,TasksController.update);
  app.delete('/tasks/:taskId',isAdmin,TasksController.remove);

  app.get('/subtasks/:subtaskId',isValid,SubTasksController.retrieve);
  app.get('/subtasks',isValid, SubTasksController.list);
  app.post('/subtasks/create',isAdmin,SubTasksController.create);
  app.post('/subtasks/:subtaskId',isValid,SubTasksController.update);
  app.delete('/subtasks/:subtaskId',isAdmin,SubTasksController.remove);

  app.get('/stages',isValid,StagesController.list);
  app.post('/stages/create',isAdmin,StagesController.create);
  app.post('/stages/:stageId',isAdmin,StagesController.update);
  app.delete('/stages/:stageId',isAdmin,StagesController.remove);

  app.get('/logs/:taskId',isValid,TasksController.getlog);

};
