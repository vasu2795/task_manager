const users = require('./users');
const tasks = require('./tasks');
const stages = require('./stages');
const subtasks = require('./subtasks');

module.exports = {
  users,
  tasks,
  stages,
  subtasks
};
