const ErrorMap = require("../utils/utils.js").ErrorMap
const SuccessMap = require("../utils/utils.js").SuccessMap
const isDate = require("../utils/utils.js").isDate
const users = require('../models').users;
const tasks = require('../models').tasks;
const isValidRequest = require('./apihelper').isValidRequest
const subtasks = require('../models').subtasks;
const stages = require('../models').stages;
const R = require('ramda');
const mapSubtasks = subtasks => {
  return subtasks.map(subtask => {
    return mapSubtask(subtask)
  });
}

const mapSubtask = subtask => {
    return {
      id: subtask.id,
      heading: subtask.heading,
      description: subtask.description,
      task_id: subtask.task_id,
      task: subtask.task ? subtask.task.heading : null,
      stage_id: subtask.stage_id,
      stage: subtask.stage ? subtask.stage.stage : null,
      username: subtask.user ? subtask.user.username : null,
      user_id: subtask.user_id
    }
}

module.exports = {
  create(req, res) {

    if(!isValidRequest("createSubTask",req))
      return res.status(404).send(ErrorMap("Invalid Params"));

    return subtasks
      .create({
        heading: req.body.heading,
        description: req.body.description,
        user_id: req.body.user_id ? parseInt(req.body.user_id) : null,
        stage_id: parseInt(req.body.stage_id),
        task_id: parseInt(req.body.stage_id)
      })
      .then(subtask => res.status(201).send(SuccessMap(subtask)))
      .catch(error => res.status(400).send(ErrorMap(error)));

  },
  retrieve(req, res) {

    if(!req.params.subtaskId)
      return res.status(404).send(ErrorMap('subtask Not Found'))

    return subtasks
      .findById(req.params.subtaskId,{
        include: [
          {model: stages, as: 'stage'},
          {model: users, as: 'user'},
          {model: tasks, as: 'task'}
        ]
      })
      .then((subtask) => {
        if (!subtask) {
          return res.status(404).send(ErrorMap('sub task Not Found'))
        }
        return res.status(200).send(mapSubtask(subtask));
      })
      .catch((error) => res.status(400).send(ErrorMap(error)));
  },
  list(req,res) {
    return subtasks
      .findAll({
        include: [
          {model: stages, as: 'stage'},
          {model: users, as: 'user'},
          {model: tasks, as: 'task'}
        ]
      })
      .then((subtasks) => {
        res.status(200).send(mapSubtasks(subtasks));
      })
      .catch((error) => res.status(400).send(ErrorMap(error)));
  },
  update(req,res) {
    if(!isValidRequest("updateSubTask",req))
      return res.status(404).send(ErrorMap("Invalid Params"));

    return subtasks
      .find({
        where: {
          id: req.params.subtaskId
        }
      })
      .then(subtask => {
        if (!subtask) {
          return res.status(404).send(ErrorMap('sub task Not Found'));
        }
        if(req.body.stage_id && !(parseInt(req.body.stage_id) == subtask.stage_id)){
          if(!(subtask.user_id == parseInt(req.headers['x-userid'])))
            return res.status(404).send(ErrorMap('Only Assigned User can modify stage'));
        }

        console.log("role here is ",req.role);
        if(req.role == "USER") {
            if(R.any((param) => {
                console.log("param is ",req.body[param],subtask[param],req.body[param] && (req.body[param] != subtask[param]));
                return (req.body[param] && (req.body[param] != subtask[param]));
            },['heading','description','user_id','task_id']))
              return res.status(400).send(ErrorMap("INVALID ACCESS"));
        }

        return subtask
          .update({
            heading: req.body.heading ? req.body.heading : subtask.heading,
            description: req.body.description  ? req.body.description : subtask.description,
            user_id: req.body.user_id ? parseInt(req.body.user_id) : subtask.user_id,
            task_id: req.body.task_id ? parseInt(req.body.task_id) : subtask.task_id,
            stage_id: req.body.stage_id ? parseInt(req.body.stage_id) : subtask.stage_id
          })
          .then(updatedsubtask => {
            console.log("updated !@# task ",updatedsubtask);
            res.status(200).send(updatedsubtask)
          })
          .catch(error => res.status(400).send(ErrorMap(error)));
      })
      .catch(error => res.status(400).send(ErrorMap(error)));

  },
  remove(req, res) {
    if(!req.params.subtaskId) {
      return res.status(404).send(ErrorMap('Invalid Params'));
    }
    return subtasks
      .find({
        where: {
          id: req.params.subtaskId
        },
      })
      .then(task => {
        if (!task) {
          return res.status(404).send(ErrorMap('sub task Not Found'));
        }

        return task
          .destroy()
          .then(() => res.status(200).send(SuccessMap("Deleted sub task")))
          .catch(error => res.status(400).send(ErrorMap(error)));
      })
      .catch(error => res.status(400).send(ErrorMap(error)));
  }
};
