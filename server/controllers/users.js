const ErrorMap = require("../utils/utils.js").ErrorMap
const SuccessMap = require("../utils/utils.js").SuccessMap
const users = require('../models').users;
const tasks = require('../models').tasks;
const isValidRequest = require('./apihelper').isValidRequest
const subtasks = require('../models').subtasks;
const stages = require('../models').stages;
const getUUID = require('../utils/utils.js').getUUID
const redis   = require("redis");
const env = process.env.NODE_ENV || 'development';
const config = require(`${__dirname}/../config/config.json`)[env];
const R = require('ramda');
const sha256 = require('sha256');
const base64 = require('base-64');
const redisClient = redis.createClient({host : config.redishost, port : config.redisport});

const mapUser = user => {
  let mappedUser = {};
  mappedUser.username = user.username;
  mappedUser.email = user.email;
  mappedUser.role = user.role;
  mappedUser.tasks = R.filter((task) =>  {
                        return task.subtasks.length == 0;
                    },user.tasks);
  mappedUser.subtasks = user.subtasks;
  return mappedUser;
}

module.exports = {
  create(req, res) {

    if(!isValidRequest("createUser",req))
      return res.status(404).send(ErrorMap("Invalid Params"));

    return users
      .create({
        username: req.body.username,
        password: base64.encode(sha256(req.body.username+req.body.password)),
        role: req.body.role,
        email: req.body.email
      })
      .then(user => res.status(201).send(SuccessMap(user)))
      .catch(error => res.status(400).send(ErrorMap(error)));
  },
  retrieve(req, res) {
    if(!req.params.userId)
      return res.status(404).send(ErrorMap('user Not Found'))

    return users
      .findById(req.params.userId, {
        include: [{
          model: tasks,
          include: [{
            model: stages,
            as: 'stages',
          }, { model: subtasks, as: 'subtasks'}],
          as: 'tasks',
        },{
          model: subtasks,
          include: [ {model: stages, as: 'stage'}],
          as: 'subtasks',
        }]

      })
      .then((user) => {
        if (!user) {
          return res.status(404).send(ErrorMap('user Not Found'))
        }
        return res.status(200).send(mapUser(user));
      })
      .catch((error) => res.status(400).send(ErrorMap(error)));
  },

  login(req, res) {

    if(!isValidRequest("loginUser",req))
      return res.status(404).send(ErrorMap("Invalid Params"));

    return users
      .findOne({
        where: {
          username: req.body.username,
          password: base64.encode(sha256(req.body.username+req.body.password)),
        },
      })
      .then(user => {
        if (!user) {
          return res.status(404).send(ErrorMap('user Not Found'));
        }
        // generate random login token - store in redis for session authentication
        let loginToken =  getUUID();
        redisClient.set(user.id + "_loginToken",(loginToken + '_' + user.role), function(err,resp) {
          if(err) {
            return res.status(400).send("error setting logintoken");
          } else {
            console.log("set in redis ",user.id + "_loginToken", loginToken + '_' + user.role);
            return res.status(200).send({loginToken : loginToken, role: user.role, id: user.id});
          }
        });
      })
      .catch(error => res.status(400).send(ErrorMap(error)));
  },
  list(req,res) {
    return users
      .findAll({})
      .then((users) => res.status(200).send(SuccessMap(users)))
      .catch((error) => res.status(400).send(ErrorMap(error)));
  },
  update(req,res) {
    console.log("update user ");
    if(!isValidRequest("updateUser",req))
      return res.status(404).send(ErrorMap("Invalid Params"));

    console.log("valid request ");
    return users
      .find({
        where: {
          id: req.params.userId
        }
      })
      .then(user => {

        if (!user) {
          return res.status(404).send(ErrorMap('user Not Found'));
        }
        console.log("use found for update");

        return user
          .update({
            role: req.body.role ? req.body.role : user.role,
            username: req.body.username  ? req.body.username : user.username,
            email: req.body.email ? req.body.email : user.email
          })
          .then(updateduser => {
            console.log("user updated");

            res.status(200).send(updateduser);
          })
          .catch(error => {
            console.log("update error ");
            res.status(400).send(ErrorMap(error))
          });
      })
      .catch(error => {
        console.log("find error ");
        res.status(400).send(ErrorMap(error))
      });

  },

   remove(req, res) {
      if(!req.params.userId) {
        return res.status(404).send(ErrorMap('Invalid Params'));
      }
      return users
        .find({
          where: {
            id: req.params.userId
          },
        })
        .then(user => {
          if (!user) {
            return res.status(404).send(ErrorMap(' user Not Found'));
          }

          return user
            .destroy()
            .then(() => res.status(200).send(SuccessMap("Deleted  user")))
            .catch(error => res.status(400).send(ErrorMap(error)));
        })
        .catch(error => res.status(400).send(ErrorMap(error)));
    },
    logout(req,res){
      // remove logintoken from redis
      redisClient.del(req.headers['x-userid'] + "_loginToken");
      res.status(200).send({status : "logged_out"});
    }
};
