const isDate = require("../utils/utils.js").isDate
const R = require('ramda');
const params_checker = {
  createUser: {
    requiredBody: [
      {key: "username" , type: "string"},
      {key: "password", type: "string"},
      {key: "role", type: "enum", allowed: ["ADMIN","USER"]},
      {key: "email" , type: "string"}
    ]
  },
  loginUser: {
    requiredBody: [
      {key: "username" , type: "string"},
      {key: "password", type: "string"},
    ]
  },
  updateUser: {
    optionalBody: [
      {key: "username" , type: "string"},
      {key: "role", type: "enum", allowed: ["ADMIN","USER"]},
      {key: "email" , type: "string"}
    ],
    requiredParams: [
      { key : "userId" , type: "integer" }
    ]
  },
  createTask: {
    requiredBody: [
      {key: "stage_id" , type: "integer"},
      {key: "heading", type: "string"},
      {key: "description", type: "string"},
      {key: "due_date", type: "date"},
    ],
    optionalBody: [
      {key: "user_id" , type: "integer"}
    ]
  },
  updateTask: {
    optionalBody: [
      {key: "user_id", type: "integer"},
      {key: "heading", type: "string"},
      {key: "description", type: "string"},
      {key: "due_date", type: "date"}
    ],
    requiredParams: [
      { key : "taskId" , type: "integer" }
    ]
  },
  updateTaskStage: {
    requiredBody: [
      {key: "stage_id" , type: "integer"}
    ],
    requiredParams: [
      { key : "taskId" , type: "integer" }
    ]
  },
  createSubTask: {
    requiredBody: [
      {key: "stage_id" , type: "integer"},
      {key: "heading", type: "string"},
      {key: "description", type: "string"},
      {key: "task_id", type: "integer"},
    ],
    optionalBody: [
      {key: "user_id" , type: "integer"}
    ]
  },
  updateSubTask: {
    optionalBody: [
      {key: "stage_id" , type: "integer"},
      {key: "task_id" , type: "integer"},
      {key: "user_id", type: "integer"},
      {key: "heading", type: "string"},
      {key: "description", type: "string"}
    ],
    requiredParams: [
      { key : "subtaskId" , type: "integer" }
    ]
  },
  updateStage : {
    requiredParams: [
      { key : "stageId" , type: "integer" }
    ],
    optionalBody: [
      {key: "level" , type: "integer"},
      {key: "stage", type: "string"}
    ]
  },
  createStage : {
    requiredBody: [
      {key: "level" , type: "integer"},
      {key: "stage", type: "string"}
    ]
  }
}

const checkValidity = (req,array,optional) => {
  return R.all( param => {
            if(param.type=="integer") {
              return req[param.key] ? Number.isInteger(parseInt(req[param.key])) : optional
            }
            else if(param.type=="date")
              return req[param.key] ? isDate(new Date(req[param.key])) : optional
            else if(param.type=="string")
              return (req[param.key] || req[param.key]=="") ? req[param.key]  : optional
            else if(param.type=="enum")
              return req[param.key] ? param.allowed.indexOf(req[param.key]) > -1 : optional
            else
              return false || optional
         },array || [])
}


const isValidRequest = (api,req) => {
  let reqParams = checkValidity(req.params,params_checker[api].requiredParams,false);
  let reqBody =  checkValidity(req.body,params_checker[api].requiredBody,false);
  let optionalParams = checkValidity(req.params,params_checker[api].optionalParams,true);
  let optionalBody = checkValidity(req.body,params_checker[api].optionalBody,true);

 return reqParams && reqBody && optionalParams && optionalBody;
}

exports.isValidRequest = isValidRequest;
