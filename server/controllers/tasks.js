const ErrorMap = require("../utils/utils.js").ErrorMap
const SuccessMap = require("../utils/utils.js").SuccessMap
const isDate = require("../utils/utils.js").isDate
const users = require('../models').users;
const tasks = require('../models').tasks;
const tasklogs = require('../models').tasklogs;
const isValidRequest = require('./apihelper').isValidRequest
const subtasks = require('../models').subtasks;
const stages = require('../models').stages;
const R = require('ramda');
const models = require("../models");


const taskLog = (list,task,userId) => {
  R.map((elem) => {
    let desc = "updated " + elem  + " to " + task[elem];
    return tasklogs.create({
      description: desc,
      user_id: userId,
      task_id: task.id,
      logged_on: new Date()
    })
   },list);
}

const mapTasks = tasks => {
  return tasks.map(task => {
    return mapTask(task)
  });
}

const mapTask = task => {

    var formattedTask = {
      id: task.id,
      heading: task.heading,
      description: task.description,
      due_date: task.due_date,
      subtasks: task.subtasks
    };

    if(task.subtasks.length == 0) {

      formattedTask.stage_id  =  task.stage_id;
      formattedTask.stage  =  task.stages ? task.stages.stage : null;
      formattedTask.username = task.user ? task.user.username : null;
      formattedTask.user_id = task.user_id;
      return formattedTask;
    } else {

      formattedTask.user_id = null;
      formattedTask.username = null;

      //
      var stage = (R.head(R.take(1,R.sortBy(R.prop('level'))(R.map(subtask => {
                                                return {
                                                        id: subtask.stage.id,
                                                        stage :subtask.stage.stage,
                                                        level: subtask.stage.level
                                                       }
                                              },task.subtasks)))))

      formattedTask.stage_id = stage.id;
      formattedTask.stage = stage.stage;

      return formattedTask;

    }
}

module.exports = {
  create(req, res) {

    if(!isValidRequest("createTask",req))
      return res.status(404).send(ErrorMap("Invalid Params"));

    return tasks
        .create({
          heading: req.body.heading,
          description: req.body.description,
          due_date: new Date(req.body.due_date),
          user_id: req.body.user_id ? parseInt(req.body.user_id) : null,
          stage_id: parseInt(req.body.stage_id)
        })
        .then(task => {
          tasklogs.create({
            description: " created task " + task.heading,
            user_id: req.headers['x-userid'],
            task_id: task.id,
            logged_on: new Date()
          }).then((response) => {
            console.log("task log created for create table ");
          }).catch((error) => {
            console.log("error log  for create table ",error);
          })

          res.status(201).send(task)
        })
        .catch(error => res.status(400).send(ErrorMap(error)));

  },
  retrieve(req, res) {
    if(!req.params.taskId)
      return res.status(404).send(ErrorMap('task Not Found'))

    return tasks
      .findById(req.params.taskId, {
        include: [{
          model: subtasks,
          include: [ {model: stages, as: 'stage'}],
          as: 'subtasks',
        }, {
          model: stages,
          as: 'stages'
        },{
          model: users,
          as: 'user'
        }]

      })
      .then((task) => {
        if (!task) {
          return res.status(404).send(ErrorMap('task Not Found'))
        }
        return res.status(200).send(mapTask(task));
      })
      .catch((error) => res.status(400).send(ErrorMap(error)));
  },
  list(req,res) {
    return tasks
      .findAll({
        attributes: ['id','heading','description','due_date'],
      })
      .then((tasks) => res.status(200).send(tasks))
      .catch((error) => res.status(400).send(ErrorMap(error)));
  },
  update(req,res) {
    if(!isValidRequest("updateTask",req))
      return res.status(404).send(ErrorMap("Invalid Params"));

    return tasks
      .find({
        where: {
          id: req.params.taskId
        },
        include: [{
          model: subtasks,
          include: [ {model: stages, as: 'stage'}],
          as: 'subtasks'
        }, {
          model: stages,
          as: 'stages'
        },
        { model: users,
          as: 'user'
        }
      ]
      })
      .then(task => {

        if (!task) {
          return res.status(404).send(ErrorMap('task Not Found'));
        }

        if(task.subtasks.length > 0) {
          let mappedTask = mapTask(task);
          if(R.any((x) => !R.equals(req.body[x],mappedTask[x]),['user_id','stage_id'])) {
            return res.status(404).send(ErrorMap('Cannot Reassign or change stage for tasks which have subtasks'));
          }
        }

        if(task.subtasks.length==0 && !(parseInt(req.body.stage_id) == task.stage_id)){
          if(!(task.user_id == parseInt(req.headers['x-userid'])))
            return res.status(404).send(ErrorMap('Only Assigned User can modify stage'));
        }

        if(req.role == "USER") {
            if(R.any((param) => {
                console.log("param is ",req.body[param],task[param],req.body[param] && (req.body[param] != task[param]));
                return (req.body[param] && (req.body[param] != task[param]));
            },['heading','description','due_date','user_id']))
              return res.status(400).send(ErrorMap("INVALID ACCESS"));
        }


        return task
          .update({
            heading: req.body.heading ? req.body.heading : task.heading,
            description: req.body.description  ? req.body.description : task.description,
            user_id: req.body.user_id ? parseInt(req.body.user_id) : task.user_id,
            due_date: req.body.due_date ? new Date(req.body.due_date) : task.due_date,
            stage_id: req.body.stage_id ? parseInt(req.body.stage_id) : task.stage_id
          })
          .then(updatedtask => {
            taskLog(Object.keys(updatedtask._changed),mapTask(updatedtask),req.headers['x-userid']);
            res.status(200).send(mapTask(updatedtask));
          })
          .catch(error => res.status(400).send(ErrorMap(error)));
      })
      .catch(error => res.status(400).send(ErrorMap(error)));

  },
  remove(req, res) {

    if(!req.params.taskId) {
      return res.status(404).send(ErrorMap('Invalid Params'));
    }

    return tasks
      .find({
        where: {
          id: req.params.taskId
        },
      })
      .then(task => {
        if (!task) {
          return res.status(404).send(ErrorMap(' task Not Found'));
        }

        return task
          .destroy()
          .then(() => res.status(200).send(SuccessMap("Deleted  task")))
          .catch(error => res.status(400).send(ErrorMap(error)));
      })
      .catch(error => res.status(400).send(ErrorMap(error)));
  },
  getlog(req,res) {

    if(!req.params.taskId) {
      return res.status(404).send(ErrorMap('Invalid Params'));
    }

    return tasklogs
      .findAll({
        where: {
          task_id: req.params.taskId
        },
        include: [{ model: users, as: 'user'}]
      })
      .then((log) => {
        if (!log) {
          return res.status(404).send(ErrorMap(' log Not Found'));
        }
        res.status(200).send(SuccessMap(log))
      })
      .catch(error => res.status(400).send(ErrorMap(error)));
  }
};
