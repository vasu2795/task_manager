const ErrorMap = require("../utils/utils.js").ErrorMap
const SuccessMap = require("../utils/utils.js").SuccessMap
const stages = require('../models').stages;
const isValidRequest = require('./apihelper').isValidRequest

module.exports = {
  create(req, res) {
    if(!isValidRequest("createStage",req))
      return res.status(404).send(ErrorMap("Invalid Params"));

    return stages
      .create({
        stage: req.body.stage,
        level: parseInt(req.body.level)
      })
      .then(stage => res.status(201).send(stage))
      .catch(error => res.status(400).send(ErrorMap(error)));

  },
  list(req,res) {
    return stages
      .findAll({})
      .then((stages) => res.status(200).send(SuccessMap(stages)))
      .catch((error) => res.status(400).send(ErrorMap(error)));
  },
  update(req,res) {
    if(!isValidRequest("updateStage",req))
      return res.status(404).send(ErrorMap("Invalid Params"));

    return stages
      .find({

      })
      .then(stage => {

        if (!stage) {
          return res.status(404).send(ErrorMap('stage Not Found'));
        }

        return stage
          .update({
            stage: req.body.stage ? req.body.stage : task.stage,
            level: Number.isInteger(parseInt(req.body.level)) ? parseInt(req.body.level) : task.level
          },{
            where: {
              id: req.params.stageId
            }
          })
          .then(updatedstage => {
            res.status(200).send(updatedstage)
          })
          .catch(error => res.status(400).send(ErrorMap(error)));
      })
      .catch(error => res.status(400).send(ErrorMap(error)));
  },
  remove(req, res) {

    if(!req.params.stageId) {
      return res.status(404).send(ErrorMap('Invalid Params'));
    }
    return stages
      .find({
        where: {
          id: req.params.stageId
        },
      })
      .then(stage => {
        if (!stage) {
          return res.status(404).send(ErrorMap(' stage Not Found'));
        }

        return stage
          .destroy()
          .then(() => res.status(200).send(SuccessMap("Deleted  stage")))
          .catch(error => res.status(400).send(ErrorMap(error)));
      })
      .catch(error => res.status(400).send(ErrorMap(error)));
  }
};
