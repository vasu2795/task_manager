About:

This is a task managere app. There are two user roles - Admin and User.

Admin has access to:
  * Create, Retrieve, Update, Delete a User
  * Create, Retrieve, Update, Delete a Task
  * Create, Retrieve, Update, Delete a Stage. Every stage has a column called level which is a measure of how close it is to completion. Level is an integer from 0 to 100, with 100 referring to completed task.
  * Assign/Reassign task to any admin or user
  * Update stage of task or subtask assigned to the admin.
  * Create, Retrieve, Update, Delete a subtask. When a subtask is created for a task, the task will no more be associated with any particular user. The stage of the task would by default be the lowest level stage of all it's subtasks.
  *  View Task Summary - List of subtasks, task history etc.

User has access to:
  * View all tasks, subtasks.
  * View summary of any task, subtask.
  * Update stage of any task or subtask assigned to the user.


Steps to install:

1) Clone repo and Cd to app's root directory.
2) npm install
3) npm run start:dev
