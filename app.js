const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const redis   = require("redis");
const session = require('express-session');
const redisStore = require('connect-redis');
const env = process.env.NODE_ENV || 'development';
const config = require(`${__dirname}/server/config/config.json`)[env];
const redisClient = redis.createClient({host : config.redishost, port : config.redisport});
const path = require('path');
const publicPath = path.resolve(__dirname, 'client');

redisClient.on('ready',function() {
 console.log("Redis is ready");
});

redisClient.on('error',function() {
 console.log("Error in Redis");
});

const app = express();

app.use(express.static(publicPath));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

require('./server/routes')(app);
app.get('*', (req, res) => res.status(200).send({
  message: 'Welcome to the beginning of nothingness.',
}));

module.exports = app;
